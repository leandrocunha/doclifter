<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
                   "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<!-- lifted from mwww+man+troff by doclifter -->
<refentry>
<!-- ' t
Title: nsenter
Author: [see the "AUTHOR(S)" section]
Generator: Asciidoctor 2.0.15
Date: 2021&bsol;-06&bsol;-02
Manual: User Commands
Source: util&bsol;-linux 2.37.2
Language: English -->

<refentryinfo><date>2021-06-02</date></refentryinfo>
<refmeta>
<refentrytitle>NSENTER</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='date'>2021-06-02</refmiscinfo>
<refmiscinfo class='source'>util-linux 2.37.2</refmiscinfo>
<refmiscinfo class='manual'>User Commands</refmiscinfo>
</refmeta>
<refnamediv>
<refname>nsenter</refname>
<refpurpose>run program in different namespaces</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
<cmdsynopsis>
  <command>nsenter</command>    <arg choice='opt'><replaceable>options</replaceable></arg>
    <arg choice='opt'><arg choice='plain'><replaceable>program</replaceable></arg><arg choice='opt'><replaceable>arguments</replaceable></arg></arg>
</cmdsynopsis>
</refsynopsisdiv>


<refsect1 id='description'><title>DESCRIPTION</title>

<para>The <emphasis role='strong' remap='B'>nsenter</emphasis> command executes <emphasis remap='I'>program</emphasis> in the namespace(s) that are specified in the command-line options (described below). If <emphasis remap='I'>program</emphasis> is not given, then "${SHELL}" is run (default: <filename>/bin/sh</filename>).</para>

<para>Enterable namespaces are:</para>

<para><emphasis role='strong' remap='B'>mount namespace</emphasis></para>
<literallayout remap='RS'>
Mounting and unmounting filesystems will not affect the rest of the system, except for filesystems which are explicitly marked as shared (with <emphasis role='strong' remap='B'>mount --make-shared</emphasis>; see <filename>/proc/self/mountinfo</filename> for the <emphasis role='strong' remap='B'>shared</emphasis> flag). For further details, see <citerefentry><refentrytitle>mount_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry> and the discussion of the <emphasis role='strong' remap='B'>CLONE_NEWNS</emphasis> flag in <citerefentry><refentrytitle>clone</refentrytitle><manvolnum>2</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>UTS namespace</emphasis></para>
<literallayout remap='RS'>
Setting hostname or domainname will not affect the rest of the system. For further details, see <citerefentry><refentrytitle>uts_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>IPC namespace</emphasis></para>
<literallayout remap='RS'>
The process will have an independent namespace for POSIX message queues as well as System V message queues, semaphore sets and shared memory segments. For further details, see <citerefentry><refentrytitle>ipc_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>network namespace</emphasis></para>
<literallayout remap='RS'>
The process will have independent IPv4 and IPv6 stacks, IP routing tables, firewall rules, the <filename>/proc/net</filename> and <filename>/sys/class/net</filename> directory trees, sockets, etc. For further details, see <citerefentry><refentrytitle>network_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>PID namespace</emphasis></para>
<literallayout remap='RS'>
Children will have a set of PID to process mappings separate from the <emphasis role='strong' remap='B'>nsenter</emphasis> process. <emphasis role='strong' remap='B'>nsenter</emphasis> will fork by default if changing the PID namespace, so that the new program and its children share the same PID namespace and are visible to each other. If <emphasis role='strong' remap='B'>--no-fork</emphasis> is used, the new program will be exec&rsquo;ed without forking. For further details, see <citerefentry><refentrytitle>pid_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>user namespace</emphasis></para>
<literallayout remap='RS'>
The process will have a distinct set of UIDs, GIDs and capabilities. For further details, see <citerefentry><refentrytitle>user_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>cgroup namespace</emphasis></para>
<literallayout remap='RS'>
The process will have a virtualized view of <filename>/proc/self/cgroup</filename>, and new cgroup mounts will be rooted at the namespace cgroup root. For further details, see <citerefentry><refentrytitle>cgroup_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>time namespace</emphasis></para>
<literallayout remap='RS'>
The process can have a distinct view of <emphasis role='strong' remap='B'>CLOCK_MONOTONIC</emphasis> and/or <emphasis role='strong' remap='B'>CLOCK_BOOTTIME</emphasis> which can be changed using <filename>/proc/self/timens_offsets</filename>. For further details, see <citerefentry><refentrytitle>time_namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>.
</literallayout> <!-- remap='RE' -->

<para>Various of the options below that relate to namespaces take an optional <emphasis remap='I'>file</emphasis> argument. This should be one of the <filename>/proc/[pid]/ns/*</filename> files described in <citerefentry><refentrytitle>namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry>, or the pathname of a bind mount that was created on one of those files.</para>

<para><emphasis role='strong' remap='B'>-a</emphasis>, <emphasis role='strong' remap='B'>--all</emphasis></para>
  <blockquote remap='RS'>
<para>Enter all namespaces of the target process by the default <filename>/proc/[pid]/ns/*</filename> namespace paths. The default paths to the target process namespaces may be overwritten by namespace specific options (e.g., <emphasis role='strong' remap='B'>--all --mount</emphasis>=[<emphasis remap='I'>path</emphasis>]).</para>

<para>The user namespace will be ignored if the same as the caller&rsquo;s current user namespace. It prevents a caller that has dropped capabilities from regaining those capabilities via a call to setns(). See <citerefentry><refentrytitle>setns</refentrytitle><manvolnum>2</manvolnum></citerefentry> for more details.
  </para></blockquote> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-t</emphasis>, <emphasis role='strong' remap='B'>--target</emphasis> <emphasis remap='I'>PID</emphasis></para>
  <blockquote remap='RS'>
<para>Specify a target process to get contexts from. The paths to the contexts specified by <emphasis remap='I'>pid</emphasis> are:</para>

<para><filename>/proc/pid/ns/mnt</filename></para>
  <blockquote remap='RS'>
<para>the mount namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/uts</filename></para>
  <blockquote remap='RS'>
<para>the UTS namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/ipc</filename></para>
  <blockquote remap='RS'>
<para>the IPC namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/net</filename></para>
  <blockquote remap='RS'>
<para>the network namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/pid</filename></para>
  <blockquote remap='RS'>
<para>the PID namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/user</filename></para>
  <blockquote remap='RS'>
<para>the user namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/cgroup</filename></para>
  <blockquote remap='RS'>
<para>the cgroup namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/ns/time</filename></para>
  <blockquote remap='RS'>
<para>the time namespace
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/root</filename></para>
  <blockquote remap='RS'>
<para>the root directory
    </para></blockquote> <!-- remap='RE' -->

<para><filename>/proc/pid/cwd</filename></para>
  <blockquote remap='RS'>
<para>the working directory respectively
    </para></blockquote> <!-- remap='RE' -->
  </blockquote> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-m</emphasis>, <emphasis role='strong' remap='B'>--mount</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the mount namespace. If no file is specified, enter the mount namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the mount namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-u</emphasis>, <emphasis role='strong' remap='B'>--uts</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the UTS namespace. If no file is specified, enter the UTS namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the UTS namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-i</emphasis>, <emphasis role='strong' remap='B'>--ipc</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the IPC namespace. If no file is specified, enter the IPC namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the IPC namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-n</emphasis>, <emphasis role='strong' remap='B'>--net</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the network namespace. If no file is specified, enter the network namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the network namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-p</emphasis>, <emphasis role='strong' remap='B'>--pid</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the PID namespace. If no file is specified, enter the PID namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the PID namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-U</emphasis>, <emphasis role='strong' remap='B'>--user</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the user namespace. If no file is specified, enter the user namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the user namespace specified by <emphasis remap='I'>file</emphasis>. See also the <emphasis role='strong' remap='B'>--setuid</emphasis> and <emphasis role='strong' remap='B'>--setgid</emphasis> options.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-C</emphasis>, <emphasis role='strong' remap='B'>--cgroup</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the cgroup namespace. If no file is specified, enter the cgroup namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the cgroup namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-T</emphasis>, <emphasis role='strong' remap='B'>--time</emphasis>[=<emphasis remap='I'>file</emphasis>]</para>
<literallayout remap='RS'>
Enter the time namespace. If no file is specified, enter the time namespace of the target process. If <emphasis remap='I'>file</emphasis> is specified, enter the time namespace specified by <emphasis remap='I'>file</emphasis>.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-G</emphasis>, <emphasis role='strong' remap='B'>--setgid</emphasis> <emphasis remap='I'>gid</emphasis></para>
<literallayout remap='RS'>
Set the group ID which will be used in the entered namespace and drop supplementary groups. <emphasis role='strong' remap='B'>nsenter</emphasis> always sets GID for user namespaces, the default is 0.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-S</emphasis>, <emphasis role='strong' remap='B'>--setuid</emphasis> <emphasis remap='I'>uid</emphasis></para>
<literallayout remap='RS'>
Set the user ID which will be used in the entered namespace. <emphasis role='strong' remap='B'>nsenter</emphasis> always sets UID for user namespaces, the default is 0.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>--preserve-credentials</emphasis></para>
<literallayout remap='RS'>
Don&rsquo;t modify UID and GID when enter user namespace. The default is to drops supplementary groups and sets GID and UID to 0.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-r</emphasis>, <emphasis role='strong' remap='B'>--root</emphasis>[=<emphasis remap='I'>directory</emphasis>]</para>
<literallayout remap='RS'>
Set the root directory. If no directory is specified, set the root directory to the root directory of the target process. If directory is specified, set the root directory to the specified directory.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-w</emphasis>, <emphasis role='strong' remap='B'>--wd</emphasis>[=<emphasis remap='I'>directory</emphasis>]</para>
<literallayout remap='RS'>
Set the working directory. If no directory is specified, set the working directory to the working directory of the target process. If directory is specified, set the working directory to the specified directory.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-F</emphasis>, <emphasis role='strong' remap='B'>--no-fork</emphasis></para>
<literallayout remap='RS'>
Do not fork before exec&rsquo;ing the specified program. By default, when entering a PID namespace, <emphasis role='strong' remap='B'>nsenter</emphasis> calls <emphasis role='strong' remap='B'>fork</emphasis> before calling <emphasis role='strong' remap='B'>exec</emphasis> so that any children will also be in the newly entered PID namespace.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-Z</emphasis>, <emphasis role='strong' remap='B'>--follow-context</emphasis></para>
<literallayout remap='RS'>
Set the SELinux security context used for executing a new process according to already running process specified by <emphasis role='strong' remap='B'>--target</emphasis> PID. (The util-linux has to be compiled with SELinux support otherwise the option is unavailable.)
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-V</emphasis>, <emphasis role='strong' remap='B'>--version</emphasis></para>
<literallayout remap='RS'>
Display version information and exit.
</literallayout> <!-- remap='RE' -->

<para><emphasis role='strong' remap='B'>-h</emphasis>, <emphasis role='strong' remap='B'>--help</emphasis></para>
<literallayout remap='RS'>
Display help text and exit.
</literallayout> <!-- remap='RE' -->
</refsect1>

<refsect1 id='authors'><title>AUTHORS</title>

<para><ulink url="mailto:biederm&commat;xmission.com">Eric Biederman</ulink>,
<ulink url="mailto:kzak&commat;redhat.com">Karel Zak</ulink></para>
</refsect1>

<refsect1 id='see_also'><title>SEE ALSO</title>

<para><citerefentry><refentrytitle>clone</refentrytitle><manvolnum>2</manvolnum></citerefentry>,
<citerefentry><refentrytitle>setns</refentrytitle><manvolnum>2</manvolnum></citerefentry>,
<citerefentry><refentrytitle>namespaces</refentrytitle><manvolnum>7</manvolnum></citerefentry></para>
</refsect1>

<refsect1 id='reporting_bugs'><title>REPORTING BUGS</title>

<para>For bug reports, use the issue tracker at <ulink url="https://github.com/karelzak/util-linux/issues"></ulink>.</para>
</refsect1>

<refsect1 id='availability'><title>AVAILABILITY</title>

<para>The <emphasis role='strong' remap='B'>nsenter</emphasis> command is part of the util-linux package which can be downloaded from <ulink url="https://www.kernel.org/pub/linux/utils/util-linux/">Linux Kernel Archive</ulink>.</para>
</refsect1>
</refentry>

